context("pseudo_closure")

universe <- head(letters, n = 5L)
n1 <- matrix(
  c(
    TRUE, FALSE, FALSE, FALSE, FALSE,
    TRUE, TRUE, FALSE, FALSE, FALSE,
    TRUE, FALSE, TRUE, FALSE, FALSE,
    FALSE, TRUE, FALSE, TRUE, FALSE,
    FALSE, TRUE, TRUE, FALSE, TRUE
  ),
  nrow = 5L, ncol = 5L, byrow = TRUE
)
q1 <- neighborhoods_predicate(n1)
empty_dnf <- dnf_predicate(list(), list(q1))
dnf <- dnf_predicate(list(1L), list(q1))

empty_space <- logical_pretopological_space(universe, empty_dnf)
space <- logical_pretopological_space(universe, dnf)

test_that("set is included in its pseudo-closure", {
  combn(length(universe), 2L, function(set) {
    res1 <- pseudo_closure(empty_space, set)
    res2 <- pseudo_closure(space, set)

    expect_true(all(res1[set]))
    expect_true(all(res2[set]))
  })
})

test_that("pseudo_closure works", {
  set <- "a"
  expect_identical(pseudo_closure(space, set), c(TRUE, TRUE, TRUE, FALSE, FALSE))

  set <- "b"
  expect_identical(pseudo_closure(space, set), c(FALSE, TRUE, FALSE, TRUE, TRUE))

  set <- c("b", "c")
  expect_identical(pseudo_closure(space, set), c(FALSE, TRUE, TRUE, TRUE, TRUE))
})


test_that("closure works", {
  set <- "a"
  expect_identical(closure(space, set), c(TRUE, TRUE, TRUE, TRUE, TRUE))

  set <- "b"
  expect_identical(closure(space, set), c(FALSE, TRUE, FALSE, TRUE, TRUE))

  set <- c("b", "c")
  expect_identical(closure(space, set), c(FALSE, TRUE, TRUE, TRUE, TRUE))
})